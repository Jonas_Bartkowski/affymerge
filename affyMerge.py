from structure import MRGFile, SpikeInFile, SUMFile, TTESTFile, CellFile, CDFFile, TTESTROCFile
import re, fileinput, sys
from optparse import OptionParser
import os


import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import warnings; warnings.filterwarnings(action='once')
from numpy.polynomial.polynomial import polyfit

def_output_folder_path = "/home/jonas_bartkowski/Studium/Aufgaben/WS 18 19/GEXP/4.1/"
def_cdf_path = "/home/jonas_bartkowski/Studium/Aufgaben/WS 18 19/GEXP/4.1/HG-U133A_tag.CDF"
def_cdf_path_win = "E:/Downloads/ttest_test/HG-U133A_tag.CDF"
def_cell_folder_path = "/home/jonas_bartkowski/Studium/Aufgaben/WS 18 19/GEXP/4.1/CEL/"
def_mrg_input_path = def_output_folder_path
def_output_folder_desc = "the resulting MRG files"
def_spikein_path = "/home/jonas_bartkowski/Studium/Aufgaben/WS 18 19/GEXP/4.1/05-spikein-tabelle.txt"
def_spikein_path_win = "E:/Downloads/ttest_test/05-spikein-tabelle.txt"
ttest_test_path = "/home/jonas_bartkowski/Studium/Aufgaben/WS 18 19/GEXP/4.1/ttest_test/"
ttest_test_path_win = "E:/Downloads/ttest_test/"


def choose_path(desc, isFolder=False, default=None, quit_key="q", def_key="d"):
    folder_path = input("Please enter a target "+("folder " if isFolder else "file ")+"for the "+desc+", or enter "+("nothing" if def_key == "" else def_key)+(" to use the default path\n"+default+"\n" if default is not None else "\n")+"Enter "+quit_key+" to quit!")
    if not folder_path == quit_key:
        folder_path = folder_path if (not folder_path == def_key or default is None) else default
        print("Choosing path", folder_path) 
        return folder_path
    else: 
        return quit_key

def test_normalization(format=".*\\.CEL"):
    choosen_path = choose_path(def_output_folder_desc, isFolder=True)
    if not choosen_path == "q":
        mrg_file = MRGFile.initialize_many_from_cell_cdf(def_cdf_path, def_cell_folder_path, cell_file_format=format, output_folder=def_output_folder_path)[0] 
        mrg_file.normalize_mean()
        mrg_file.write_to_file(def_output_folder_path+"normalized.mrg")

def test_summation(mrg_file_format=".*\\.(mrg|MRG)"):
    opath = choose_path("the resulting sum files", default=def_output_folder_path)
    mpath = choose_path("the target mrg files", default=def_mrg_input_path)
    mrg_files = MRGFile.load_many_from_folder(mpath, mrg_file_format=mrg_file_format)
    print("Loaded",len(mrg_files),"valid mrg_files")
    print("Converting to sum!")
    i = 0
    for mrg_file in mrg_files:
        sum_file = mrg_file.to_sum()
        sum_file.write_to_file(def_output_folder_path+str(i)+".sum")
        i=i+1


def test_read_mrg():
    choosen_path = choose_path(def_output_folder_desc, isFolder=True)
    if not choosen_path == "q":
        MRGFile.initialize_many_from_cell_cdf(
                        def_cdf_path,
                        def_cell_folder_path,
                        cell_file_format=".*\.CEL",
                        output_folder=choosen_path,
                        returnFiles=False)
        print("Done!")

#Should yield 16 pms for gene 37004_at in CDF
#regex: 37004_at\t\d+\t\d+\t((G\tC\tG)|(A\tT\tA)|(T\tA\tT)|(C\tG\tC))
def test_create_single_mrg(cell_file_format):
    MRGFile.initialize_many_from_cell_cdf(
                def_cdf_path,
                def_cell_folder_path,
                cell_file_format=cell_file_format,
                output_folder=def_output_folder_path,
                returnFiles=False)

def test_load_spikein():
    path = choose_path("The spike-in file", default=def_spikein_path)
    #print("Selected path:", path)
    spike_in: SpikeInFile = SpikeInFile.from_file(path)
    print("Found "+str(len(spike_in.entries))+" entries in loaded spikein file!")

def test_ttest(create_new_mrg=False, create_new_ttest=False, calculate_variability=True, plot_fcexp_vs_fccalc=True, plot_roc=True, calculate_not_spiked_false_positives=True, plot_psd_vs_fccalc_fcexp=True, rounding_digits=4, debug_level=0):
    #breakpoint()
    #breakpoint()
    spike_in: SpikeInFile = SpikeInFile.from_file(def_spikein_path_win)
    #breakpoint()
    print("Loaded spikein file with", len(spike_in.entries), "entries!")
    if create_new_ttest:
        if create_new_mrg:
            mrg_files = MRGFile.initialize_many_from_cell_cdf(def_cdf_path_win, ttest_test_path_win, normalize=True, output_folder=ttest_test_path_win)
            print("Compiled and normalized", len(mrg_files), "mrg files!")
            print("Converting to sum and saving!")
        else:
            mrg_files = MRGFile.load_many_from_folder(ttest_test_path_win)
        sum_files = MRGFile.many_to_sum(mrg_files, output_folder=ttest_test_path_win)
        mid = int(len(sum_files)/2)
        conda = sum_files[0:mid]
        condb = sum_files[mid:len(sum_files)]
        #breakpoint()
        print("We have",len(sum_files),"sum files. mid =",mid)
        print("Creating ttest files!")
        files_dict = TTESTFile.many_from_sum(conda, condb, spike_in, output_folder=ttest_test_path_win+os.path.sep+"ttests", apply_logarithm=True, rounding_digits=rounding_digits)
        files_list = [file for file in files_dict.values()]
    else:
        #files_list = TTESTFile.many_from_folder(ttest_test_path_win+os.path.sep+"ttests")
        files_dict = TTESTFile.many_from_folder_dict(ttest_test_path_win+os.path.sep+"ttests")
        files_list = [file for file in files_dict.values()]
        print("Loaded", len(files_list), "ttest files!")
    
    if calculate_variability:
        s = TTESTFile.calculate_variability(files_list, spike_in)
        print("1.6.1: Varibility s =", s)
    if plot_fcexp_vs_fccalc:
        TTESTFile.plot_fcexp_vs_fccalc(files_list, spike_in)
    if plot_roc:
        tvs = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08]
        ttest_roc_file = TTESTROCFile.eval_ttest("test", files_dict, spike_in, tvs, debug_level=debug_level)
        ttest_roc_file.plot_roc(files_dict, spike_in, tvs)
    if calculate_not_spiked_false_positives:
        fp = TTESTFile.not_spiked_false_positives(files_list, spike_in)
        print("1.6.4: Not spiked false positives:", fp)
    if plot_psd_vs_fccalc_fcexp:
        TTESTFile.plot_fcexp_fccalc_vs_psda_psdb(files_list, spike_in)
    
    breakpoint()
    
    print("Done!")

def test_make_scatplot():
    spike_in = SpikeInFile.from_file(def_spikein_path_win)
    ttest_files = TTESTFile.many_from_folder(ttest_test_path_win)
    print("Loaded", len(ttest_files), "TTEST files...")
    TTESTFile.plot_fcexp_vs_fccalc(ttest_files, spike_in)

def get_first_line_matching(lines, regex):
    for line in lines:
        if re.match(regex, line, flags=re.IGNORECASE):
            return line
    return None


def process_instructions(lines: [str], debug_level=0):
    lines = [line.replace("\n","") for line in lines]
    if debug_level:
        print("Got", len(lines), "lines:", lines)
    inss = [line.strip() for line in lines]
    if debug_level:
        print("ins:", inss)
    if inss[0] == "[AFFYMERGE-INSTRUCTIONS]":
        if debug_level:
            print("Header detected!")
        inss.pop(0)
        cell_folder_path = get_first_line_matching(lines, "cel=.+")
        if (cell_folder_path is not None):
            cell_folder_path = re.sub("cel=", "", cell_folder_path)
        cdf_file_path = get_first_line_matching(lines, "cdf=.+")
        if (cdf_file_path is not None):
            cdf_file_path = re.sub("cdf=", "", cdf_file_path)
        do_mrg = get_first_line_matching(lines, "do_mrg") is not None
        do_norm = get_first_line_matching(lines, "do_norm") is not None
        output_dir = get_first_line_matching(lines, "out_dir=.+")
        if (output_dir is not None):
            output_dir = re.sub("out_dir=", "", output_dir)
        do_sum = get_first_line_matching(lines, "do_sum") is not None
        do_ttest = get_first_line_matching(lines, "do_ttest") is not None
        spike_in_path = get_first_line_matching(lines, "spike_in=.+")
        if (spike_in_path is not None):
            spike_in_path = re.sub("spike_in=", "", spike_in_path)
        present_ttest_results = get_first_line_matching(lines, "present_ttest_results") is not None
        
        if cell_folder_path is not None:
            if os.path.isdir(cell_folder_path):
                print("Cell folder detected!")
                cell_files = CellFile.load_many_from_folder(cell_directory=cell_folder_path)
            else:
                print("Given cell path is no directory!")
        else:
            print("No cell folder path given!")
            return
        
        if cdf_file_path is not None and re.match(".+.cdf", cdf_file_path, flags=re.IGNORECASE):
            print("CDF file detected!")
            cdf_file = CDFFile(cdf_file_path)
        else:
            print("No cdf file given!")
            return

        sum_files = []
        mrg_files = []
        if do_mrg:
            print("Starting merge process for", len(cell_files), "cell files!")
            for cell_file in cell_files:
                mrg_file = MRGFile.initialize_from_cell_cdf(cdf_file, cell_file, normalize=do_norm)
                mrg_file.write_to_folder(output_dir)
                mrg_files.append(mrg_file)
                if (do_sum):
                    print("Converting mrg_file", mrg_file.name, "to sum file!")
                    sum_file = mrg_file.to_sum()
                    sum_file.write_to_folder(output_dir)
                    sum_files.append(sum_file)

        ttest_files = {}
        if do_ttest:
            if len(sum_files) > 0:
                mid = int(len(sum_files)/2)
                conda = sum_files[0:mid]
                condb = sum_files[mid:len(sum_files)]
                if spike_in_path is not None:
                    spike_in = SpikeInFile.from_file(spike_in_path)
                ttest_files = TTESTFile.many_from_sum(conda, condb, spike_in, output_folder=output_dir)
                        
        if present_ttest_results:
            files_list = [tfile for tfile in ttest_files.values()]
            s = TTESTFile.calculate_variability(files_list, spike_in)
            print("Varibility s =", s)
            print("Plotting fcexp vs fccalc")
            TTESTFile.plot_fcexp_vs_fccalc(files_list, spike_in)
            fp = TTESTFile.not_spiked_false_positives(files_list, spike_in)
            print("Not spiked false positives:", fp)
            print("Plotting fcexp_fccalc vs psda+psdb")
            TTESTFile.plot_fcexp_fccalc_vs_psda_psdb(files_list, spike_in)
            thresholds = get_first_line_matching(lines, "thresholds=.+")
            if thresholds is not None:
                thresholds = re.sub("out_dir=", "", thresholds)
                thresholds = thresholds.replace("[").replace("]")
                thresholds = thresholds.split(",")
                thresholds = [float(ts) for ts in thresholds]
                ttest_roc_file = TTESTROCFile.eval_ttest("TEST TTEST", ttest_files, spike_in, thresholds)
                ttest_roc_file.plot_roc(ttest_files, spike_in, thresholds)
            
        print("DONE!")
        return True
    return False

def main_menu():
    cell_files = {}
    c = None
    while c is not "q":
        c = input("Hello! Enter cel to load cell files, cdf to load cdf files mrg to mrg, norm to normalize, sum to sum, sin to load a spike-in file or ttest to create a ttest file. Enter q to quit.")
        if c is "cel":
            pass
        elif c is "cdf":
            pass
        elif c is "mrg":
            pass
        elif c is "norm":
            pass
        elif c is "sum":
            pass
        elif c is "sin":
            pass
        elif c is "ttest":
            pass

def test_scatter_3():
    dx = np.asarray([-1,-1,1,4,5,6,7,8,9])
    dy = np.asarray([-10,-10,10, 12, 36, 76, 12, 78, 43])
    sns.set_style("white")
    sns.regplot(dx, dy)
    plt.xlabel("FCEXP")
    plt.ylabel("FCCALC")
    plt.show()

def main():
    '''
    parser = OptionParser()
    parser.add_option("-p", "--pipe", action="store_true", dest="use_pipe", help="Read instructions from stdin")
    (options, args) = parser.parse_args(sys.argv)
    if options.use_pipe:
        lines = sys.stdin.readlines()
        print("Read",len(lines),"lines!")
        if (len(lines) > 0):
            process_instructions(lines)
        else:
            print("No lines to read!")
            print("...")
            input()
    '''
    path = input("Please choose a path to load instructions or enter nothing to continue with test mode!")
    if path is not "":
        with open(path, "r") as file:
            process_instructions(file.readlines(), debug_level=1)
    else:
        #test_summation(mrg_file_format="normalized.mrg")
        #test_create_single_mrg("Expt1.*\\.CEL")
        #test_normalization(format="Expt1_R1\\.CEL")
        #test_load_spikein()
        test_ttest(create_new_ttest=False, rounding_digits=4, debug_level=1)
        #test_scatter_3()
        #test_scatter_2()
        #test_make_scatplot()

if __name__ == "__main__":
    main()
