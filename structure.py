import re
import os
from os import listdir
from os.path import isfile, join
from scipy import stats
import statistics
import math
import datetime


import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import warnings; warnings.filterwarnings(action='once')

class CDFFile:

    def __init__(self, cdf_file_path):
        self.cdf_lines = {}
        with open(cdf_file_path, "r") as cdf_file:
            line_s = cdf_file.read()
            line_s = re.sub(".*(?=CellHeader)", "", line_s)
            lines = line_s.split("\n")
            print("Loaded cdf file with",len(lines),"lines!\nCompiling data\n....");
            v_lines = 0
            for i in range(1, len(lines)):
                line = lines[i]
                if not re.match("^CellHeader", line):
                    line = re.sub("Cell\d+=", "", line)
                    vals = re.split("\s+", line)
                    vals = list(filter(None, vals))
                    if len(vals) >= 15:
                        v_lines += 1
                        coord = (int(vals[0]), int(vals[1]))
                        #print("coord: x =", vals[0], "y =", vals[1])
                        probe = vals[2]
                        feat = vals[3]
                        qual = vals[4]
                        expos = int(vals[5])
                        pos = int(vals[6])
                        cbase = vals[7]
                        pbase = vals[8]
                        tbase = vals[9]
                        atom = int(vals[10])
                        index = int(vals[11])
                        codon_id = int(vals[12])
                        codon = int(vals[13])
                        region_type = int(vals[14])
                        cdf_line = CDFLine(coord[0], coord[1], probe, feat, qual, expos, pos, cbase, pbase, tbase, atom, index, codon_id, codon, region_type)
                        self.cdf_lines[coord] = cdf_line

class CDFLine:

    def __init__(self, x, y, probe, feat, qual, expos, pos, cbase, pbase, tbase, atom, index, codon_id, codon, region_type):
        self.coord = (x, y)
        self.probe = probe
        self.feat = feat
        self.qual = qual
        self.expos = expos
        self.pos = pos
        self.cbase = cbase
        self.pbase = pbase
        self.tbase = tbase
        self.atom = atom
        self.index = index
        self.codon_id = codon_id
        self.codon = codon
        self.region_type = region_type

class CellFile:

    def __init__(self, cell_path):
        self.cell_lines = {}
        with open(cell_path, "r") as cell_file:
            self.name = os.path.basename(cell_path).replace(".CEL","").replace(".cel", "")
            line_s = cell_file.read()
            #line_s = re.sub(".*CellHeader=", "", line_s)
            lines = line_s.split("\n")
            for i in range(1, len(lines)):
                vals = re.split("\s+", lines[i])
                vals = list(filter(None, vals))
                if len(vals) == 5:
                    try:
                        x = vals[0]
                        y = vals[1]
                        coord = (int(x), int(y))
                        mean = vals[2].strip()
                        stdv = vals[3].strip()
                        n_pixels = vals[4].strip()
                        self.cell_lines[coord] = CellLine(coord[0], coord[1], mean, stdv, n_pixels)
                    except ValueError:
                        pass

            print("Found", len(self.cell_lines), "valid lines for this cell file!")

    @staticmethod
    def load_many_from_folder(cell_directory, cell_file_format=".*\.CEL"):
        cell_files = []
        count = 0
        for f in listdir(cell_directory):
            if isfile(join(cell_directory, f)) and re.match(cell_file_format, f):
                print("Loading valid cell files...", count+1,"\n....")
                count += 1
                cell_file = CellFile(join(cell_directory, f))
                cell_files.append(cell_file)
        return cell_files
                


class CellLine:

    def __init__(self, x, y, mean, stdv, n_pixels):
        self.coord = (x,y)
        self.mean = float(mean)
        self.stdv = float(stdv)
        self.n_pixels = int(n_pixels)

class MRGLine:

    def __init__(self, coord, mean, gene, atom, cbase_pbase_tbase, pm_mm):
        self.coord = coord
        self.mean = mean
        self.gene = gene
        self.atom = atom
        self.cbase_pbase_tbase = cbase_pbase_tbase
        self.pm_mm = pm_mm

    @staticmethod
    def from_cell_and_cdf(cell_line, cdf_file, check_validity=True):
        #print("The CDF file has", len(cdf_file.cdf_lines), "lines.")

        if not check_validity or cell_line.coord in cdf_file.cdf_lines:
            cdf_line = cdf_file.cdf_lines[cell_line.coord]
            coord = cell_line.coord
            mean = cell_line.mean
            gene = cdf_line.qual
            atom = cdf_line.atom
            cbase_pbase_tbase = cdf_line.cbase + cdf_line.pbase + cdf_line.tbase
            pm_mm = "PM" if (cbase_pbase_tbase in ("GCG","ATA","TAT","CGC")) else "MM"
            return MRGLine(coord, mean, gene, atom, cbase_pbase_tbase, pm_mm)
        else:
            raise ValueError("valid merge file needs to have matching coordinate in cdf file and cell line.")

    def to_line(self):
        return str(self.coord[0]) + "\t" + str(self.coord[1]) + "\t" + str(self.mean) + "\t" \
               +self.gene + "\t" + str(self.atom) + "\t" + self.cbase_pbase_tbase + "\t" \
               +self.pm_mm

    def copy(self):
        return MRGLine(self.coord, self.mean, self.gene, self.atom, self.cbase_pbase_tbase, self.pm_mm)

class MRGFile:

    def __init__(self, name: str, mrg_lines: {MRGLine}, normalized = False):
        self.name = name
        self.mrg_lines = mrg_lines
        self.normalized = normalized

    @staticmethod
    def initialize_from_cell_cdf_paths(cdf_path, cell_path, output_path=None):
        return MRGFile.initialize_from_cell_cdf(CDFFile(cdf_path), CellFile(cell_path), output_file=output_path)

    @staticmethod
    def initialize_from_cell_cdf(cdf_file, cell_file, output_file=None, normalize=False):
        mrg_lines = {}
        for coord, cline in cell_file.cell_lines.items():
            if (coord in cdf_file.cdf_lines):
                mrg_lines[coord] = MRGLine.from_cell_and_cdf(cline, cdf_file, check_validity=False)

        mfile = MRGFile(cell_file.name, mrg_lines)
        if (normalize):
            mfile.normalize_mean()

        if (output_file != None):
            print("Writing merge file to", output_file)
            mfile.write_to_file(output_file)
        
        return mfile

    @staticmethod
    def initialize_many_from_cell_cdf(cdf_path, cell_directory, cell_file_format=".*\.CEL", output_folder=None, returnFiles=True, normalize=False):
        print("Loading cdf_file from", cdf_path,"\n....")
        cdf_file = CDFFile(cdf_path)
        print("Done! CDF file has,",len(cdf_file.cdf_lines),"valid lines!\n")

        print("Found", len(listdir(cell_directory)), " files in target dir! Loading valid files!\n")

        mrg_files = []
        count = 0
        cell_files = CellFile.load_many_from_folder(cell_directory, cell_file_format=cell_file_format)
        for cell_file in cell_files:
            file_path = output_folder
            if file_path is not None:
                file_path += "" if (output_folder.endswith("/") or output_folder.endswith("\\")) else "/"
                file_path += cell_file.name + (".mrg" if not normalize else ".norm.mrg")
            
            mrg_file = MRGFile.initialize_from_cell_cdf(cdf_file, cell_file, output_file=file_path, normalize=normalize)
            
            if (returnFiles):
                mrg_files.append(mrg_file)

        '''for f in listdir(cell_directory):
            if isfile(join(cell_directory, f)) and re.match(cell_file_format, f):
                print("Loading valid cell file and converting to merge...", count+1,"\n....")
                count += 1
                cell_file = CellFile(join(cell_directory, f))
                file_path = output_folder
                if (file_path != None):
                    file_path += "" if (output_folder.endswith("/") or output_folder.endswith("\\")) else "/"
                    file_path += cell_file.name + (".mrg" if not normalize else ".norm.mrg")
                
                mrg_file = MRGFile.initialize_from_cell_cdf(cdf_file, cell_file, output_file=file_path, normalize=normalize)
                
                if (returnFiles):
                    mrg_files.append(mrg_file)
        '''

        print("Done","Compiling and Saving" if (output_folder is not None and returnFiles) else "Compiling" if (output_folder is None and returnFiles) else "Saving",count,"valid merge files!\n")

        return mrg_files

    def write_to_folder(self, folder_path):
        if (os.path.isdir(folder_path)):
            filepath = folder_path + os.path.sep + self.name + (".mrg" if not self.normalized else ".norm.mrg")
            self.write_to_file(filepath)
        else:
            err = "Given folder '"+folder_path+"' is no folder!"
            raise ValueError(err)

    def write_to_file(self, filepath):
        with open(filepath, "w") as t_file:
            if (self.normalized):
                t_file.write("NORMALIZED\n")
            t_file.write("x\ty\tMEAN\tGENE\tATOM\tCBASE/PBASE/TBASE\tPM/MM\n")
            for coord, mline in self.mrg_lines.items():
                t_file.write(mline.to_line() + "\n")

    @staticmethod
    def load_from_file(path):
        with open(path, "r") as mrg_file:
            mrg_string = mrg_file.read()
            lines = mrg_string.split("\n")
            mrg_lines = {}
            normalized = False
            if (lines[0] == "NORMALIZED"):
                normalized = True
            for line in lines:
                vals = line.split("\t")
                if len(vals) == 7:
                    try:
                        x = int(vals[0])
                        y = int(vals[1])
                        coord = (x, y)
                        mean = float(vals[2])
                        gene = vals[3]
                        atom = int(vals[4])
                        cbase_tbase_pbase = vals[5]
                        pm_mm = vals[6]
                        mrg_lines[coord] = MRGLine(coord, mean, gene, atom, cbase_tbase_pbase, pm_mm)
                    except ValueError:
                        pass
        if (len(mrg_lines) > 0):
            return MRGFile(os.path.basename(path).replace(".norm.mrg", "").replace(".mrg", ""), mrg_lines, normalized=normalized)

        raise ValueError("Invalid merge file (0 valid lines)")

    @staticmethod
    def load_many_from_folder(mrg_dir, mrg_file_format=".*\.(mrg)"):
        print("Found", len(listdir(mrg_dir)), "files in target dir! Loading valid files!\n")

        mrg_files = []
        count = 0
        for f in listdir(mrg_dir):
            apath = join(mrg_dir, f) 
            if isfile(apath) and re.match(mrg_file_format, f):
                print("Loading valid mrg file", count+1," [",f,"]....")
                count += 1
                mrg_files.append(MRGFile.load_from_file(apath))
                
        return mrg_files

    def correct_background_signal(self, radius, bg_percentile):
        new_lines = {}
        count = 1
        for mrg_line in self.mrg_lines.values():
            new_lines[mrg_line.coord] = mrg_line.copy()
            new_lines[mrg_line.coord].mean = self.collect_mean_background_signal(mrg_line.coord, radius, bg_percentile)
            print(count, ". line done!", sep="")
            count += 1

        self.mrg_lines = new_lines
        return self

    def collect_mean_background_signal(self, base_coordinate, radius, bg_percentile):
        means = {}
        for y in range(-radius, radius, 1):
            if y != 0:
                for x in range(-radius, radius, 1):
                    if x != 0:
                        coord = (base_coordinate.x + x, base_coordinate.y + y)
                        if coord in self.mrg_lines:
                            means[coord] = self.mrg_lines[coord].mean

        bg_means = sorted(means.items(), key = lambda kv : kv[1])
        bg_means = bg_means[0:int(len(bg_means)*bg_percentile)]
        return (sum(val) for val in bg_means) / len(bg_means)

    def normalize_mean(self, stretch_factor=200):
        if not self.normalized:
            signals = sorted(map(lambda line : line.mean, self.mrg_lines.values()))
            c_mean = signals[int(len(signals)/2)]
            print("Mean for merge file is:", c_mean)
            for mrg_line in self.mrg_lines.values():
                n_mean = int((mrg_line.mean / c_mean) * stretch_factor)
                mrg_line.mean = n_mean
            self.normalized = True
        else:
            print("ERROR: Cannot normalize already normalized file!")
    
    def to_sum(self, output_folder=None):
        #check name and size of sfile
        sfile = SUMFile(self.name, mrg_file=self)
        if (output_folder is not None):
            sfile.write_to_file(output_folder)
        return sfile

    def gene_keyed_dict(self):
        gdict = {}
        for mline in self.mrg_lines.values():
            gene = mline.gene
            if gene in gdict:
                gdict[gene].append(mline)
            else:
                gdict[gene] = [mline]
        return gdict

    @staticmethod
    def many_to_sum(mrg_files, output_folder=None):
        sum_files = []
        i = 1
        for mfile in mrg_files:
            print("Converting mrg file ",i,"/",len(mrg_files)," ...", sep="")
            sum_file: SUMFile = mfile.to_sum()
            sum_files.append(sum_file)
            if output_folder is not None:
                sum_file.write_to_file(output_folder)
                print("Saved sum file ",i,"/",len(mrg_files),"!", sep="")
            i+=1
        return sum_files

class SUMLine:

    def __init__(self, gene, signal, psd):
        self.gene = gene
        self.signal = signal
        self.psd = psd

    def __str__(self):
        return self.gene+"\t"+str(self.signal)+"\t"+str(self.psd)+"\n"

class SUMFile:

    def __init__(self, name, sum_lines=None, mrg_file=None, debug=False):
        self.sum_lines = []
        self.name = name
        if sum_lines is not None:
            self.sum_lines = sum_lines
        elif mrg_file is not None: 
            # does self.sum_lines already contain sum_lines here? Yes!
            gene_exp_dict = {} 
            for mrg_line in mrg_file.mrg_lines.values():
                gene = mrg_line.gene
                mean = mrg_line.mean
                pmmm = mrg_line.pm_mm
                if debug:
                    print("pmmm = "+pmmm)
                if pmmm == "PM":
                    if gene in gene_exp_dict:
                        gene_exp_dict[gene].append(mean)
                    else:
                        gene_exp_dict[gene] = [mean]

            # gene_exp_dict actually only contains the 22k entries
            # self.sum_lines already contains entries here, wtf?!
            c = 0
            for gene, means in gene_exp_dict.items():
                #print("Found", len(means), "entries for", gene)
                means.sort()
                sum_line = SUMLine(gene, statistics.median(means), statistics.stdev(means))
                self.sum_lines.append(sum_line)
                #print("sum_line [GENE SIGNAL PSD]:", str(sum_line), "means =", means)
                #breakbreakpoint()
                c+=1
        print("Done initializing sum file!")
    
    def write_to_file(self, output_folder: str):
        #check length and name of sumfile
        filepath = output_folder + os.path.sep
        filepath += self.name + ".sum"
        with open(filepath, "w") as t_file:
            t_file.write("GENE\tSIGNAL\tPSD\n")
            for line in self.sum_lines:
                t_file.write(str(line))

    def write_to_folder(self, folder_path):
        if (os.path.isdir(folder_path)):
            self.write_to_file(folder_path)
        else:
            err = "Given folder '"+folder_path+"' is no folder!"
            raise ValueError(err)

    def __str__(self):
        return self.name+".sum"

class SpikeInFile:

    def __init__(self, num_exps, entries: {}):
        for entry in entries.values():
            if len(entry.exps) is not num_exps:
                error: str = "Each SpikeInEntry needs to contain exactly "+str(num_exps)+" entries! (was "+str(len(entry.exps))+")"
                raise ValueError(error)
        self.entries = entries
        self.num_exps = num_exps

    def is_spiked(self, gene):
        return gene in self.entries

    def get_spiked_genes(self):
        return [gene for gene in self.entries]

    @staticmethod
    def from_file(file):
        lines = []
        entries = {}
        with open(file) as spike_in:
            for line in spike_in:
                values = re.split("\t", line)
                lines.append(values)

        num_exps = len(lines)-2
        print("Found", num_exps, "experiments!")
        for i in range(1, len(lines[0])-1):
            id = lines[0][i]
            gene_id = lines[1][i]
            genes = re.split("\s", gene_id.replace("\"", ""))
            exps = []
            num_exps = len(lines)-2
            for j in range(2, len(lines)):
                exp = lines[j][i]
                exps.append(float(exp))
            for gene in genes:
                entries[gene]=(SpikeInEntry(id, gene, exps))

        return SpikeInFile(num_exps, entries)

class SpikeInEntry:

    def __init__(self, group_id, gene: str, exps: [float]):
        self.group_id = group_id
        self.gene = gene
        self.exps = exps

class TTESTLine:

    @staticmethod
    def calculate(gene, sigs1, psds1, conc1, sigs2, psds2, conc2, apply_logarithm=True, debug_level=0):
        gene = gene
        conc1 = conc1 if conc1 is not None else None
        mw1 = sum(sigs1) / len(sigs1)
        sigs1 = [math.log(sig) for sig in sigs1] if apply_logarithm else sigs1
        sd1 = statistics.stdev(sigs1) if len(sigs1) > 1 else 0
        conc2 = conc2 if conc2 is not None else None
        mw2 = sum(sigs2) / len(sigs2)
        sigs2 = [math.log(sig) for sig in sigs2] if apply_logarithm else sigs2
        sd2 = statistics.stdev(sigs2) if len(sigs2) > 1 else 0

        conc1 = conc1 if conc1 is not None and conc1 > 0.0 else None
        conc2 = conc2 if conc2 is not None and conc2 > 0.0 else None
        fccalc = mw1 / mw2
        if conc1 == None or conc2 == None:
            fcexp = None
        else:
            fcexp = conc1 / conc2
            if debug_level > 0:
                print("fcexp for gene", gene, "is", fcexp, "with conc1 =", conc1, "and conc2 =", conc2)

        #This doesn't happen. The
        if fcexp is 0:
            print("fcexp = 0 for gene", gene, "!")
            input()

        p = stats.ttest_rel(sigs1, sigs2, nan_policy='omit').pvalue
        return TTESTLine(gene, conc1, sigs1, psds1, mw1, sd1, conc2, sigs2, psds2, mw2, sd2, fccalc, fcexp, p)

    def __init__(self, gene: str, conc1: float, sigs1: [float], psds1: [float], mw1: float, sd1: float, conc2: float, sigs2: [float], psds2: [float], mw2: float, sd2: float, fccalc: float, fcexp: float, p: float):
        self.gene = gene
        self.conc1 = conc1
        self.sigs1 = sigs1
        self.psds1 = psds1
        self.mw1 = mw1
        self.sd1 = sd1
        self.conc2 = conc2
        self.sigs2 = sigs2
        self.psds2 = psds2
        self.mw2 = mw2
        self.sd2 = sd2
        self.fccalc = fccalc
        self.fcexp = fcexp
        self.p = p
       
        '''
        sigs1nans = [math.isnan(x) for x in self.sigs1]
        sigs2nans = [math.isnan(x) for x in self.sigs2]    
        if len(sigs1nans) > 0:   
            print("nanss in sigs1:", len(sigs1nans), sigs1nans)
        if len(sigs2nans) > 0:
            print("nanss in sigs2:", len(sigs2nans), sigs2nans)
        # ‘propagate’ returns nan, ‘raise’ throws an error, ‘omit’ performs the calculations ignoring nan values.
        '''

    def to_line(self, rounding_digits=4):
        ds = rounding_digits
        sep = "\t"
        sigs1 = [round(sig, ds) for sig in self.sigs1]
        psds1 = [round(psd, ) for psd in self.psds1]
        sigs2 = [round(sig, ds) for sig in self.sigs2]
        psds2 = [round(psd, ds) for psd in self.psds2]
        conc1 = round(self.conc1, ds) if self.conc1 is not None else None
        conc2 = round(self.conc2, ds) if self.conc2 is not None else None
        fcexp = round(self.fcexp, ds) if self.fcexp is not None else None
        return self.gene+sep+str(conc1)+sep+str(sigs1)+sep+str(psds1)+sep+str(round(self.mw1, ds))+sep+str(round(self.sd1, ds))+sep+str(conc2)+sep+str(sigs2)+sep+str(psds2)+sep+str(round(self.mw2, ds))+sep+str(round(self.sd2, ds))+sep+str(round(self.fccalc, ds))+sep+str(fcexp)+sep+str(round(self.p, ds))+"\n"


    @staticmethod
    def from_line(line):
        line.replace("\n", "")
        values = line.split("\t")
        gene = values[0]
        conc1 = None if values[1] == "None" else float(values[1])
        sigs1 = str_to_float_list(values[2])
        psds1 = str_to_float_list(values[3])
        mw1 = float(values[4])
        sd1 = float(values[5])
        conc2 = None if values[6] == "None" else float(values[6])
        sigs2 = str_to_float_list(values[7])
        psds2 = str_to_float_list(values[8])
        mw2 = float(values[9])
        sd2 = float(values[10])
        fccalc = float(values[11])
        fcexp = None if values[12] == "None" else float(values[12])
        p = float(values[13])
        return TTESTLine(gene, conc1, sigs1, psds1, mw1, sd1, conc2, sigs2, psds2, mw2, sd2, fccalc, fcexp, p)


class TTESTFile:

    ttest_header = "GENE\tCONC1\tSIGS1\tPSDS1\tMW1\tSD1\tCONC2\tSIGS2\tPSDS2\tMW2\tSD2\tFCCALC\tFCEXP\tp\n"

    def __init__(self, name, ttest_lines: {}):
        self.name = name
        self.ttest_lines = ttest_lines

    @staticmethod
    def many_from_sum(sum_files_conda: [SUMFile], sum_files_condb: [SUMFile], spike_in: SpikeInFile, output_folder=None, apply_logarithm=True, rounding_digits=4):
        if (len(sum_files_conda) < 2 or len(sum_files_condb) < 2):
            raise ValueError("Generating TTEST files from sum files requires at least 2 files for both conditions!")

        namesa = [sfile.name for sfile in sum_files_conda]
        namesb = [sfile.name for sfile in sum_files_condb]
        print("Compiling TTEST data for", namesa, "as conda and", namesb, "as condb!", end=" ")

        gene_lines_conda = {}
        gene_lines_condb = {}
        # for all sum files and their lines, compute a dict
        # that has gene of that line as key and all the lines
        # that belong to that gene as values.
        # conda and condb are differentiated by two dicts.
        # the random false is not conda or condb
        #print(len(sum_files_conda),"conds a",len(sum_files_condb),"conds b")
        for sum_file in sum_files_conda:
            for sum_line in sum_file.sum_lines:
                gene = sum_line.gene
                #print(gene)
                if gene in gene_lines_conda.keys():
                    gene_lines_conda[gene].append(sum_line)
                else:
                    gene_lines_conda[gene] = [sum_line]

        for sum_file in sum_files_condb:
            for sum_line in sum_file.sum_lines:
                gene = sum_line.gene
                #print(gene)
                if gene in gene_lines_condb:
                    gene_lines_condb[gene].append(sum_line)
                else:
                    gene_lines_condb[gene] = [sum_line]
        #print(gene_count_conda)
        #print(gene_count_condb)

        #Find all genes that have entries in condition a and condition b
        #neither is there any false in these condsa and condsb
        intersects = [k for k in gene_lines_conda if k in gene_lines_condb]

        #for gene in intersects:
            #print(len(gene_lines_conda[gene]),"entries for", gene, "in conda and",len(gene_lines_condb[gene]),"entries in condb")

        #compute two dicts for both conditions which have
        #gene as key, where sigs has all signal entries 
        #for a particular gene as value and psds has all
        #psd entries as values.
        sigs1 = {}
        psds1 = {}
        sigs2 = {}
        psds2 = {}
        for gene in intersects:     
            sigs1[gene] = [line.signal for line in gene_lines_conda[gene]]
            psds1[gene] = [line.psd for line in gene_lines_conda[gene]]
            sigs2[gene] = [line.signal for line in gene_lines_condb[gene]]
            psds2[gene] = [line.psd for line in gene_lines_condb[gene]]
            #rint("We have",len(sum_lines_a),"sum_lines_a and",len(sum_lines_b),"sum_lines_b!")
            '''
            sum_lines_a: [SUMLine] = gene_lines_conda[gene]
            sum_lines_b: [SUMLine] = gene_lines_condb[gene]

            sig1 = [line.signal for line in sum_lines_a]
            psd1 = [line.psd for line in sum_lines_a]
            sig2 = [line.signal for line in sum_lines_b]
            psd2 = [line.psd for line in sum_lines_b]
            
            sig1 = []
            psd2 = []
            for sum_line_a in sum_lines_a:
                sig1.append(sum_line_a.signal)
                psd1.append(sum_line_a.psd)
            sig2 = []
            psd2 = []
            for sum_line_b in sum_lines_b:
                sig2.append(sum_line_b.signal)
                psd2.append(sum_line_b.psd)

            sigs1[gene]=sig1
            sigs2[gene]=sig2
            psds1[gene]=psd1
            psds2[gene]=psd2
            '''
            
            

        #compute a dict with the gene as key
        #and the associated concentrations as value.
        exp_values = {}
        for gene, entry in spike_in.entries.items():
            print("GENE ='", gene, "'", sep="")
            exp_values[gene] = {}
            for i in range(0, len(entry.exps)):
                exp_values[gene][i] = float(entry.exps[i])

        #compute a dict where for every gene the following
        #is associated:
        #the indecies of all valid experiment comparisons as tupels as keys 
        #(i.e. (0,1) (0,2) (0,3)), 
        #and their associated
        #concentrations as keys.
        concs = {}
        for gene in intersects:
            concs[gene] = {}
            #for i in exp_values[gene].keys():
            for i in range(0, spike_in.num_exps):
                #for j in exp_values[gene].keys():
                for j in range(0, spike_in.num_exps):
                    if (i < j):
                        if gene in exp_values:
                            concs[gene][(i,j)] = (exp_values[gene][i], exp_values[gene][j])
                        else:
                            concs[gene][(i,j)] = None

        #Create a reverse dictionary where every
        #experiment (i.e. 1 v 2, 1 v 3) is associated
        #with a list that contains the associated concentration
        #and the associated genes.
        expdata = {}
        for gene, exps in concs.items():
            for exp, concs in exps.items():
                if exp in expdata:
                    expdata[exp].append((concs, gene))
                else:
                    expdata[exp] = [(concs, gene)]


        print("Done! Compiling ttest files!")

        #for each experiment create a ttest_file 
        #where every concentration+gene tupel corresponds
        #to a line in th resulting ttest_file.
        ttest_files = {}
        #91 expdata entries
        for exp, conc_genes in expdata.items():
            #breakpoint()
            print("Compiling ttest file for exp", exp[0]+1, " v exp", exp[1]+1, "... ", sep="", end='')
            #breakpoint()
            ttest_lines = {}
            #This loop the major speed hog
            #distinct_conc_genes = list(dict.fromkeys(conc_genes))
            #print("len conc genes =", len(conc_genes), "len distinct conc genes =", len(distinct_conc_genes))
            #22300 concentrations
            for conc_gene in conc_genes:
                conc = conc_gene[0]
                gene = conc_gene[1]
                #breakpoint()
                #print("conc_gene =", conc_gene, "conc =", conc, "gene =", gene)
                #This is not a speed hog
                ttest_line = TTESTLine.calculate(gene, sigs1[gene], psds1[gene], conc[0] if conc is not None else None, sigs2[gene], psds2[gene], conc[1] if conc is not None else None, apply_logarithm=apply_logarithm)
                if gene in ttest_lines:
                    ttest_lines[gene].append(ttest_line)
                else:
                    ttest_lines[gene] = [ttest_line]

            #breakpoint()
            ttest_file = TTESTFile("EXP"+str(exp[0]+1)+"v"+"EXP"+str(exp[1]+1), ttest_lines)
            #breakpoint()
            ttest_files[exp] = ttest_file
            print("Done! ", end="")
            if output_folder is not None:
                print("Writing to file... ", end='')
                #ttest_file.writeToFile(output_folder+os.path.sep+"TTEST_"+datetime.datetime.now().strftime("%Y-%m-%d-%H:%M")+os.pathsep)
                ttest_file.writeToFile(output_folder, rounding_digits=rounding_digits)
                print("Done!")
            
        #For every comparison of experiments create a ttest file
        #that for every line has an associated gene and the 
        #required associated data for that gene as to the specification
        #of a ttest line.
        #for i in range(concs.values()[0].values()):
        #    for gene in intersects:
        #        for comp, conc12 in concs[gene].items():
        #            ttest_lines = []
        #            ttest_line = TTESTLine(gene, sigs1[gene], psd1[gene], conc12[0], sigs2[gene], psd2[gene], conc12[1])
        #            ttest_lines += ttest_line
        #
        #            ttest_file = TTESTFile(ttest_lines)
        #            ttest_files[comp] = ttest_file
        #
        return ttest_files

    def writeToFile(self, output_folder: str, rounding_digits=4):
        sep = os.path.sep
        final_path = (output_folder + self.name + ".ttest") if output_folder.endswith(sep) else (output_folder + sep + self.name + ".ttest")
        with open(final_path, "w") as file:
            file.write(TTESTFile.ttest_header)
            lines = []
            for ttest_lines in self.ttest_lines.values():
                for ttest_line in ttest_lines:
                    lines.append(ttest_line.to_line(rounding_digits=rounding_digits))
            file.writelines(lines)

    def gene_keyed_dict(self):
        gdict = {}
        for ttlines in self.ttest_lines.values():
            for ttline in ttlines:
                gene = ttline.gene
                if gene in gdict:
                    gdict[gene].append(ttline)
                else:
                    gdict[gene] = [ttline]
        return gdict

    @staticmethod
    def from_file(file_path):
        ttest_lines = {}
        name = re.sub("/\.ttest/gi", "", os.path.basename(file_path))
        with open(file_path, "r") as file:
            c = 0
            for line in file:
                if not c is 0:
                    ttest_line: TTESTLine = TTESTLine.from_line(line)
                    gene = ttest_line.gene
                    if gene in ttest_lines:
                        ttest_lines[gene].append(ttest_line)
                    else:
                        ttest_lines[gene] = [ttest_line]
                c+=1
        return TTESTFile(name, ttest_lines)

    @staticmethod
    def many_from_folder(folder_path, ttest_file_format=".+\.ttest"):
        ttest_files = []
        count = 0
        for f in listdir(folder_path):
            path = join(folder_path, f)
            if isfile(path) and re.match(ttest_file_format, f, flags=re.IGNORECASE):
                count += 1
                ttest_file = TTESTFile.from_file(path)
                print("Loaded valid ttest file (",len(ttest_file.ttest_lines.values())," genes) ...", count+1,"\n", sep="")
                ttest_files.append(ttest_file)
        return ttest_files

    @staticmethod
    def many_from_folder_dict(folder_path, ttest_file_format=".+\.ttest"):
        ttest_files = {}
        count = 0
        for f in listdir(folder_path):
            path = join(folder_path, f)
            if isfile(path) and re.match(ttest_file_format, f, flags=re.IGNORECASE):
                count += 1
                ttest_file = TTESTFile.from_file(path)
                print("Loaded valid ttest file '", f, "' (",len(ttest_file.ttest_lines.values())," genes) ... ", count+1,"\n", sep="")
                exps = f.replace(".ttest", "").replace("EXP", "").split("v")
                exps = [int(exp) for exp in exps]
                exps = (exps[0], exps[1])
                ttest_files[exps] = ttest_file
        return ttest_files

    @staticmethod
    def plot_fcexp_vs_fccalc(ttest_files: [], spike_in: SpikeInFile, mode=0):
        fcexps = []
        fccalcs = []
        genes = [gene for gene in spike_in.entries]
        print("Plotting scatter plot for", len(genes), "genes and", len(ttest_files), "ttest files!")
        
        concs = []
        for entry in spike_in.entries.values():
            for exp in entry.exps:
                concs.append(exp)

        concs = list(dict.fromkeys(sorted(concs)))
        concs = [conc for conc in concs if concs.index(conc) > 2 and concs.index(conc) < len(concs) - 3]        
        #breakpoint()

        for ttest_file in ttest_files:
            for gene in genes:
                ttest_lines = ttest_file.ttest_lines[gene]
                for ttest_line in ttest_lines:
                    conc1 = ttest_line.conc1
                    conc2 = ttest_line.conc2
                    if conc1 in concs and conc2 in concs:
                        fcexp = ttest_line.fcexp
                        fccalc = ttest_line.fccalc
                        if fcexp is not None:
                            if (fcexp == 0):
                                print("fcexp 0 on ttest file", ttest_file.name, "and gene", gene)
                            fcexp = fcexp if fcexp >= 1 else -1/fcexp
                            fccalc = fccalc if fccalc >= 1 else -1/fccalc
                            fcexps.append(fcexp)
                            if mode is 0:
                                fccalcs.append(fccalc)
                            elif mode is 1:
                                fccalcs.append(fcexp / fccalc)
                            elif mode is 2:
                                fccalcs.append(fccalc / fcexp)

        sns.set_style("white")
        sns.regplot(fcexps, fccalcs)
        plt.xlabel("FCEXP")
        plt.ylabel("FCCALC")
        breakpoint()
        plt.show()

    @staticmethod
    def calculate_variability(ttest_files: [], spike_in: SpikeInFile):
        genes_fcces = {}
        genes = [gene for gene in spike_in.entries]
        
        concs = []
        for entry in spike_in.entries.values():
            for exp in entry.exps:
                concs.append(exp)

        concs = list(dict.fromkeys(sorted(concs)))
        concs = [conc for conc in concs if concs.index(conc) > 2 and concs.index(conc) < len(concs) - 3]    

        for ttest_file in ttest_files:
            for gene in genes:
                ttest_lines = ttest_file.ttest_lines[gene]
                for ttest_line in ttest_lines:
                    conc1 = ttest_line.conc1
                    conc2 = ttest_line.conc2
                    if conc1 in concs and conc2 in concs:
                        fcexp = ttest_line.fcexp
                        fccalc = ttest_line.fccalc
                        if fcexp is not None:
                            if gene in genes_fcces:
                                genes_fcces[gene].append((fccalc, fcexp))
                            else:
                                genes_fcces[gene] =[(fccalc, fcexp)]
        
        s = 0
        n = 0
        for gene, fcces in genes_fcces.items():
            n += len(fcces)
            for fcce in fcces:
                fcc = fcce[0]
                fce = fcce[1]
                s += abs(fcc-fce)
        
        s /= n
        return s

    @staticmethod
    def not_spiked_false_positives(ttest_files: [], spike_in: SpikeInFile):
        genes_lines_dicts = [ttest_file.gene_keyed_dict() for ttest_file in ttest_files]

        diff_exp = 0
        for genes_lines_dict in genes_lines_dicts:
            for gene, g_lines in genes_lines_dict.items():
                if not spike_in.is_spiked(gene):
                    for g_line in g_lines:
                        p = g_line.p
                        fcc = g_line.fccalc
                        if p < 0.01 and fcc > 1.5:
                            diff_exp += 1

        return diff_exp

    @staticmethod
    def plot_fcexp_fccalc_vs_psda_psdb(ttest_files: [], spike_in: SpikeInFile, mode=0):
        fccalc_fcexp = []
        psda_psdb = []
        genes = [gene for gene in spike_in.entries]
        #breakpoint()

        print("BLUBLUBLUB")
        for ttest_file in ttest_files:
            for gene in genes:
                ttest_lines = ttest_file.ttest_lines[gene]
                for ttest_line in ttest_lines:
                    fccalc = ttest_line.fccalc
                    fcexp = ttest_line.fcexp
                    sd1 = ttest_line.sd1
                    sd2 = ttest_line.sd2
                    if fcexp is not None:
                        try:
                            if mode is 0:
                                fccalc_fcexp.append(fccalc / fcexp)
                            elif mode is 1:
                                fccalc_fcexp.append(fccalc - fcexp)
                            elif mode is 2:
                                fccalc_fcexp.append((fccalc - fcexp) / fccalc)
                            psda_psdb.append(sd1+sd2)
                        except:
                            print("fcexp = 0 for gene", gene, "in ttest file", ttest_file.name)


        sns.set_style("white")
        sns.regplot(fccalc_fcexp, psda_psdb)
        plt.xlabel("FCCALC vs FCEXP")
        plt.ylabel("PSDA + PSDB")
        breakpoint()
        plt.show()

class TTESTROCLine:

    def __init__(self, exp, fractions: [float]):
        self.exp = exp
        self.fractions = fractions

    def __str__(self):
        ln = self.exp[0]+"vs"+self.exp[1]+"\t"
        for frac in self.fractions:
            ln += round(frac, 2) + "\t"
        return ln


class TTESTROCFile:

    def __init__(self, name, tttest_roc_lines: {(int, int), TTESTROCLine}, mws: [float], threshold_values: [float]):
        self.name = name
        self.tttest_roc_lines = tttest_roc_lines
        self.mws = mws
        
    def plot_roc(self, ttest_files: {(int, int), TTESTFile}, spike_in: SpikeInFile, threshold_values: [float]):
        pfps = []
        ptps = self.mws

        spiked_genes = spike_in.get_spiked_genes()

        concs = []
        for entry in spike_in.entries.values():
            for exp in entry.exps:
                concs.append(exp)

        concs = list(dict.fromkeys(sorted(concs)))
        concs = [conc for conc in concs if concs.index(conc) > 2 and concs.index(conc) < len(concs) - 3]     
        breakpoint()   

        #false positives sollte das Gegenteil von true positives sein
        #cond = ttest_line.p < tv and test_line.conc1 is None and ttest_line.conc2 is None:

        for tv in threshold_values:
            num = 0
            fps = 0
            for ttest_file in ttest_files.values():
                for gene, ttest_lines in ttest_file.ttest_lines.items():
                    for ttest_line in ttest_lines:
                        if gene not in spiked_genes:
                            p = ttest_line.p
                            conc1 = ttest_line.conc1
                            conc2 = ttest_line.conc2
                            if (conc1 is None and conc2 is None):
                                num += 1
                                if p < tv:
                                    fps += 1
            
            pfps.append(fps/num)

        data = pd.DataFrame({"P(FP)": pfps, "P(TP)": ptps})
        #sns.set_style("white")
        sns.set()
        sns.lineplot(x="P(FP)", y="P(TP)", data=data)
        #plt.xlabel("P(FP)")
        #plt.ylabel("P(FP)")
        breakpoint()
        plt.show()

    def write_to_file(self, folder_path):
        path = folder_path + os.path.sep + self.name + ".ttest.roc"
        with open(path, "w") as file:
            header = "Exp\t"
            for i in range(self.mws):
                header += "tp-"+'{0:03}'.format(i)
            file.write(header)
            lines = []
            for ttest_roc_line in self.tttest_roc_lines.values():
                lines.append(str(ttest_roc_line))

            file.writelines(lines)
            mws_l = "MW\t"
            for mw in self.mws:
                mws_l += mw + "\t"
            
            file.write(mws_l)


    @staticmethod
    def eval_ttest(name, ttest_files: {(int, int), TTESTFile}, spike_in: SpikeInFile, threshold_values: [float], debug_level=2):
        spiked_genes = spike_in.get_spiked_genes()
        fractions: {(int, int), [float]} = {}

        concs = []
        for entry in spike_in.entries.values():
            for exp in entry.exps:
                concs.append(exp)

        concs = list(dict.fromkeys(sorted(concs)))
        concs = [conc for conc in concs if concs.index(conc) > 2 and concs.index(conc) < len(concs) - 3]

        mws = []
        tvi = 0
        for tv in threshold_values:
            for exp, ttest_file in ttest_files.items():
                if debug_level:
                    print("Comparing", ttest_file.name, "for ROC plot!")
                num_spiked_genes = 0
                true_positives = 0
                for gene in spiked_genes:
                    if debug_level > 1:
                        print("Comparing gene", gene)
                    ttest_lines = ttest_file.ttest_lines[gene]
                    for ttest_line in ttest_lines:
                        num_spiked_genes += 1
                        # if is true positive
                        if ttest_line.conc1 is not None and ttest_line.conc2 is not None:
                            if ttest_line.p < tv and ttest_line.fcexp >= ttest_line.fccalc and ttest_line.conc1 in concs and ttest_line.conc2 in concs:
                                true_positives += 1
                fraction = true_positives / num_spiked_genes
                if exp in fractions:
                    fractions[exp].append(fraction)
                else:
                    fractions[exp] = [fraction]
           
            val = 0
            for fracs_exp in fractions.values():        
                val += fracs_exp[tvi]
            mw = val / len(ttest_files)
            mws.append(mw)
            tvi += 1

                #ttest_roc_line = TTESTROCLine(exp, )

        if debug_level:
            print("MWS=", mws)

        ttest_roc_lines: {(int, int), TTESTROCLine} = {}
        for exp, fracs_exp in fractions:
            ttest_roc_line: TTESTROCLine = TTESTROCLine(exp, fracs_exp)
            ttest_roc_lines[exp] = ttest_roc_line
        
        return TTESTROCFile(name, ttest_roc_lines, mws, threshold_values)

def str_to_float_list(str):
        return [float(sig) for sig in re.sub("[\\[\\]\\s]", "", str).split(",")]